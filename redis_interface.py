import redis
import numpy as np
from functools import reduce
from tqdm import tqdm

with open("redispassword.txt") as file:
    REDIS = redis.Redis(host='redis-16383.c77.eu-west-1-1.ec2.cloud.redislabs.com', port=16383, db=0,
                        password=next(file))


def translate_matrix(state):
    return '\n'.join(''.join('.xrgyb'[x] for x in y) for y in state)


def translate_string(string):
    s = []
    for x in string.split('\n'):
        v = []
        for y in x:
            v.append('.xrgyb'.index(y))
        s.append(v)
    return np.array(s)


def translate_colors(lst):
    s = ''
    for x, y in zip('rgby', lst):
        if y:
            s = s + x
    return s


class Puzzle:
    def __init__(self, index, row, col):
        self.index = index
        self.row = row
        self.col = col
        self.key = f"puzzle:{self.index}:{self.row}:{self.col}"

    def get_puzzle(self):
        try:
            return translate_string(REDIS.get(self.key + ':puzzle').decode('utf-8'))
        except AttributeError:
            return None

    def get_puzzle_raw(self):
        try:
            return REDIS.get(self.key + ':puzzle').decode('utf-8')
        except AttributeError:
            return None

    def set_puzzle(self, value):
        value = translate_matrix(value)
        print(value)
        return REDIS.set(self.key + ':puzzle', value)

    def get_solution(self):
        try:
            return REDIS.get(self.key + ':solution').decode('utf-8')
        except AttributeError:
            try:
                #print('load alt solution')
                return REDIS.get(self.key + ':solution_alt').decode('utf-8')
            except AttributeError:
                return None

    def mark_sol_as_alt(self):
        if REDIS.get(self.key + ':solution') is None:
            print('solution already marked as alternate')
        else:
            self.set_solution(self.get_solution())
            REDIS.delete(self.key + ':solution')


    def set_solution(self, value):
        return REDIS.set(self.key + ':solution_alt', value)

    def get_colors(self):
        try:
            return REDIS.get(self.key + ':colors').decode('utf-8')
        except AttributeError:
            return None

    def set_colors(self, value):
        return REDIS.set(self.key + ':colors', translate_colors(value))


def combine_right(s1, s2):
    return '\n'.join(x + y for x, y in zip(s1.split('\n'), s2.split('\n')))


def combine(index):
    yield f'prt {index} | \n'
    count = 0
    for i in range(5):
        for j in range(5):
            p = Puzzle(index, i, j).get_puzzle_raw() is not None
            s = Puzzle(index, i, j).get_solution() is not None
            yield 'V' if s else '?' if p else '.'
            count += s
        yield ' | \n'
    yield f'{count:<5} | \n'

def print_all():
    for index in range(8):
        for i in range(5):
            for j in range(5):
                #print(Puzzle(index, i, j).get_puzzle_raw())
                print(index,i,j)
                print(Puzzle(index, i, j).get_colors())
                print(Puzzle(index, i, j).get_solution())



if __name__ == "__main__":
    print_all()

    print(reduce(combine_right, tqdm((''.join(combine(x)) for x in range(7)), total=8)))
