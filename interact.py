import retro
from time import sleep
from redis_interface import Puzzle, translate_matrix
from subprocess import run
import argparse
import numpy as np


class Environment():
    b = 0
    select = 2
    start = 3
    up = 4
    down = 5
    left = 6
    right = 7
    a = 8

    def __init__(self, state='startstate2'):

        self.state = state
        self.env = retro.make('..\\contrib\\DenkiBlocks', self.state, use_restricted_actions=retro.Actions.ALL,
                              record='.')
        # print(self.env.unwrapped.buttons)

    def __enter__(self):
        self.env.reset()
        self.env.record_movie(self.state + '.bk2')
        return self

    def __exit__(self, *_):
        self.env.reset()
        self.env.close()

    @staticmethod
    def playback(state, file=...):
        if file is ...:
            file = state + '.bk2'
        movie = retro.Movie(file)
        movie.step()

        env = retro.make(
            game=movie.get_game(),
            state=state,
            # bk2s can contain any button presses, so allow everything
            use_restricted_actions=retro.Actions.ALL,
            players=movie.players,
        )
        env.initial_state = movie.get_state()
        env.reset()
        env.render()

        # print('test0')
        while movie.step():
            # print('test')
            keys = []
            for p in range(movie.players):
                for i in range(env.num_buttons):
                    keys.append(movie.get_key(i, p))
            env.step(keys)
            sleep(1 / 60)
            env.render()

    def send_button(self, button=-1, ticks=10, rep=0):
        #if button > -1:
        #    print(self.env.unwrapped.buttons[button])
        action = [0] * 12
        if button > -1:
            action[button] = 1
        # print(action)
        for _ in range(ticks):
            _obs, _rew, done, _info = self.env.step(action)
            if done:
                assert False
            if rep and _ % rep == 0:
                self.env.render()
                # sleep(0.01)
        if rep:
            self.env.render()

    def get_screen(self):
        return self.env.unwrapped.get_screen()

    def ram(self, index):
        for k, v in self.env.unwrapped.data.memory.blocks.items():
            if k > index:
                continue
            rindex = index - k
            values = np.frombuffer(v, dtype=np.uint8)
            if rindex >= len(values):
                continue
            return values[rindex]

        assert False, "adress {index} not found"


translate = {
    'U': Environment.up,
    'D': Environment.down,
    'L': Environment.left,
    'R': Environment.right
}


class DenkiBlockEnv(Environment):
    def selected(self):
        return self.ram(0x3006af4)

    def get_screen_processed(self):
        s = np.sum(self.get_screen()[12::8, 12::8, :][:16, :16, :], axis=2)
        f = np.zeros((16, 16), dtype=np.int)
        # print(s)
        for i, x in enumerate([696, 296, 232, 464, 400]):
            f += (s == x) * (i + 1)
        return f

    def get_colors_final(self):
        s = np.sum(self.get_screen()[ 7:8,-53:-5, :], axis=2).reshape(4,-1)
        r = list(np.sum(s - s[:,0,np.newaxis],axis=1)> 0)
        return r

    def wait(self):
        #self.env.render()
        self.send_button(-1, 5)
        #self.env.render()
        s = self.get_screen()[8:136, 8:136, :]
        self.send_button(-1, 1)
        timeout = True
        tns = iter([4])
        mx = 10000
        while timeout:
            mx -= 1
            if mx == 0:
                break
            prev = s.copy()
            #self.env.render()
            s = self.get_screen()[8:136, 8:136, :]
            self.send_button(-1, 1)
            if np.all(prev == s):
                timeout -= 1
            else:
                try:
                    timeout = next(tns)
                except StopIteration:
                    timeout = 15

    def solve(self, allowgen):
        for i in range(26):
            self.send_button()
        for x in solve(self.get_screen_processed(), allowgen):
            # print(x)
            self.send_button(translate[x], 5)
            # self.send_button()
            self.wait()
        self.env.render()
        for i in range(50):
            self.send_button()

    def wait_for_cutscene(self):
        while True:
            try:
                self.recognise_main()
                break
            except AssertionError:
                self.send_button(self.a)
                self.send_button()
                continue

    def wait_for_cutscene2(self):
        while True:
            try:
                self.recognise_main()
                break
            except AssertionError:
                self.send_button(self.b)
                self.send_button(self.a)
                self.send_button(self.b)
                self.send_button()
                continue

    def wait_for_exit(self):
        for i in range(40):
            try:
                self.recognise_main()
                break
            except AssertionError:
                self.send_button(self.b)
                self.send_button()
                continue

    def scan_row(self, skip, amount, allowgen=False):
        for x in range(skip):
            self.send_button(self.down)
            self.send_button()
        for y in range(amount):
            for x in range(5):
                self.send_button(self.a)
                self.solve(allowgen)
                self.send_button(self.right)
            self.send_button(self.down)

    def scan(self, allowgen):
        self.scan_row(0, 3, allowgen)
        self.wait_for_cutscene(60)
        self.solve(allowgen)
        self.scan_row(3, 2, allowgen)
        self.wait_for_cutscene(20)
        self.solve(allowgen)
        # self.scan_row(0, 1, allowgen)
        self.env.render()
        self.send_button(self.b)
        self.send_button(-1, 100)
        self.send_button(self.right)
        self.send_button(-1, 100)
        self.send_button(self.a)
        self.send_button(-1, 100)
        self.wait_for_cutscene(100)
        self.env.render()
        self.solve(allowgen)

    def select_puzzle(self, puzzle):
        self.recognise_main()
        self.send_button(-1, 5)
        rshift = puzzle.index - self.selected() + 8
        for x in range(rshift%8):
            self.send_button(self.right)
            self.send_button(-1, 10)
        self.send_button(self.a)
        self.send_button(-1, 100)
        if puzzle.row < 3:
            for x in range(puzzle.row):
                self.send_button(self.down)
                self.send_button()
        else:
            for x in range(5-puzzle.row):
                self.send_button(self.up)
                self.send_button()
        if puzzle.col < 3:
            for x in range(puzzle.col):
                self.send_button(self.right)
                self.send_button()
        else:
            for x in range(5-puzzle.col):
                self.send_button(self.left)
                self.send_button()
        self.send_button(self.a)
        self.send_button(-1, 260)

    def skip_intro(self, index):
        self.recognise_main()
        self.send_button(-1, 100)
        rshift = index - self.selected() + 8
        for x in range(rshift % 8):
            self.send_button(self.right)
            self.send_button(-1, 100)
        self.send_button(self.a)
        self.send_button(-1, 100)
        self.wait_for_cutscene2()

    def store_puzzle(self, puzzle):
        self.select_puzzle(puzzle)
        mat = self.get_screen_processed()
        cols = self.get_colors_final()
        self.env.render()
        puzzle.set_puzzle(mat)
        puzzle.set_colors(cols)
        self.exit_puzzle()

    def solve_puzzle(self, puzzle):
        self.select_puzzle(puzzle)
        for x in puzzle.get_solution():
            self.send_button(translate[x], 5)
            # self.send_button()
            self.wait()
        self.env.render()
        self.wait_for_exit()
        #self.send_button(self.b)
        self.send_button(-1, 5)
        self.wait_for_cutscene()
        print('done')

    def exit_puzzle(self):
        self.send_button()
        self.send_button()
        self.send_button()
        self.send_button()
        self.send_button(self.start)
        self.send_button()
        self.send_button()
        self.send_button()
        self.send_button()
        self.send_button(self.down)
        self.send_button()
        self.send_button(self.down)
        self.send_button()
        self.send_button(self.a)
        self.send_button(-1, 200)
        self.send_button(self.b)
        self.send_button(-1, 200)
        print('done')

    def recognise_main(self):
        if "st" not in self.__dict__:
            self.st = self.get_screen()[:6, :6, :]
            self.send_button()
        assert np.all(self.st == self.get_screen()[:6, :6, :])

def main(extra=True,parse=True):
    with DenkiBlockEnv() as e:
        e.send_button(-1, 200)
        for x in range(6):
            e.skip_intro(x)
            solved = 0
            for i in range(5):
                for j in range(5):
                    p = Puzzle(x, i, j)
                    print('solved:',solved)
                    if parse:
                        e.store_puzzle(p)
                    if solved < 15 or (extra and solved < 24):
                        if p.get_solution() is not None:
                            solved += 1
                            e.solve_puzzle(p)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Create the full run')
    parser.add_argument('--parse', '-p', action='count', default=0)
    parser.add_argument('--extra', '-x', action='count', default=0)
    args = parser.parse_args()
    main(args.extra,args.parse)





# run(('python', '-m', 'retro.scripts.playback_movie', e.state + '.bk2'))
