import numpy as np
from collections import deque
from itertools import count
import tqdm
import hashlib
import base64
import heapq
from redis_interface import Puzzle
from random import shuffle
from itertools import product
from time import time

MAX_MOVES = 500

left, right, up, down = (0, -1), (0, 1), (-1, 0), (1, 0)


def check_connected(v):
    if not np.any(v):
        return True
    sx = np.argmax(v)
    deck = deque([np.unravel_index(sx, v.shape)])
    res = np.zeros(v.shape, dtype=np.bool)
    while deck:
        x, y = deck.popleft()
        res[x, y] = True
        for xx, yy in (x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1):
            if not (0 <= xx < 16 and 0 <= yy < 16):
                continue
            if (not res[xx, yy]) and v[xx, yy]:
                deck.append((xx, yy))
    # print(str(State(res!= v)))
    return np.all(res == v)


class State:
    parents = {}
    genkey = {}

    def __init__(self, ar, parent=None, genkey=None):
        self.state = np.array(ar).copy()

        self.blocked = np.zeros((16, 16), dtype=np.bool)

        assert hash(parent) in self.parents or parent is None

        if parent is None:
            self.parents[hash(self)] = None
            self.genkey[hash(self)] = None

        elif hash(self) not in self.parents or State.len_history(hash(parent)) < State.len_history(
                self.parents[hash(self)]):
            # print('val1', hash(self) not in self.parents)
            # if hash(self) in self.parents:
            #    print('val2', State.len_history(hash(parent)),State.len_history(self.parents[hash(self)]))
            self.parents[hash(self)] = hash(parent)
            self.genkey[hash(self)] = genkey
            # print(hash(self),'->',hash(parent))
            # print(State.len_history(hash(self)),State.len_history(hash(parent)))
            # print(State.len_history(hash(self)), State.len_history(hash(parent)))
            # print(self.history_string())

        # print(self)

    @staticmethod
    def get_history(hs):
        while hs is not None:
            yield State.genkey[hs]
            hs = State.parents[hs]

    @staticmethod
    def len_history(hs):
        return len(list(State.get_history(hs)))

    def history_string(self):
        G = list(map(translate.get, self.get_history(hash(self))))
        return ''.join(reversed(G[:-1]))

    def __hash__(self):
        return int(hashlib.sha1(str(self).encode('utf-8')).hexdigest(), 16)

    def __le__(self, other):
        return hash(self) < hash(other)

    def __ge__(self, other):
        return hash(self) > hash(other)

    def __str__(self):
        return '\n'.join(''.join('.xrgyb'[x] for x in y) for y in self.state) + '\n'

    def copy(self):
        return State(self.state)

    def padded(self):
        return np.pad(self.state, 1, 'constant', constant_values=1)

    def shift(self, x, y):
        return np.roll(self.padded(), (x, y), axis=(0, 1))[1:-1, 1:-1]

    def shift_block(self, x, y):
        return np.roll(np.pad(self.blocked, 1, 'constant', constant_values=1), (x, y), axis=(0, 1))[1:-1, 1:-1]

    def connect(self, x, y):
        return self.shift(x, y) == self.state

    def resolve_block(self, x, y):
        self.blocked = np.zeros((16, 16), dtype=np.bool)
        # print(self.blocked)
        self.blocked |= (self.state == 1)
        # print(self.blocked)
        for i in range(256):
            cach = self.blocked.copy()
            self.blocked |= (self.shift_block(-x, -y) & (self.state > 0))
            # print(self.blocked)
            for d in left, right, up, down:
                self.blocked |= (self.connect(*d) & self.shift_block(*d))
            if np.all(cach == self.blocked):
                break
            # print(self.blocked)
        # print(self.blocked)

    def move(self, dir):
        stay = self.state * (self.blocked)
        moved = self.shift(*dir) * (1 - self.shift_block(*dir))
        assert np.all(stay * moved == 0), "Overlap"
        return stay + moved

    def get_moves(self):
        assert not self.check_finished()
        for x in left, right, up, down:
            self.resolve_block(*x)
            s = State(self.move(x), self, x)
            yield s

    def check_connected(self, value):
        v = self.state == value
        return check_connected(v)

    def check_finished(self):
        return all(self.check_connected(x) for x in [2, 3, 4, 5])

    def check_connection(self):
        return sum(
            float(np.sum(np.nan_to_num(np.var(np.where(self.state == value), axis=1), True, 0))) for value in [2, 3, 4]) \
               + self.len_history(hash(self)) / 30


class BackTracer:
    def __init__(self, initial, maxtime):
        self.creation = time()
        self.end = time() + maxtime if maxtime else float('inf')
        self.deck = deque()
        self.q = []

        self.deck.append(initial)
        self.seenstates = set()
        self.initial = initial
        heapq.heappush(self.q, (initial.check_connection(), -1, initial))

    def filename(self):
        return hex(hash(self.initial)) + '.hs'

    def safesearch(self, allowregen=False):
        try:
            #print(self.initial)
            #print('loaded', self.filename())
            with open(self.filename()) as file:
                res = list(''.join(file))
                if len(res) > MAX_MOVES and allowregen:
                    print('RERUN')
                    raise FileNotFoundError
                #print(len(res))
                return ''.join(res)
        except FileNotFoundError:
            assert allowregen, "generation not allowed"
            #print('not saved', self.filename())
            return self.search()

    def search(self):
        bar = tqdm.tqdm(count())
        try:
            for i in bar:
                if time() > self.end:
                    assert False, "Timeout"
                if not self.q:
                    assert False, "No solution found"
                # x = self.deck.popleft()
                try:
                    a, b, x = heapq.heappop(self.q)
                except IndexError:
                    assert False, "No solution found"
                # print(a,b)
                if i % 1000 == 0:
                    # print(State.len_history(hash(x)))
                    # print(x)
                    pass
                # print(x.check_connection())
                self.seenstates.add(hash(x))
                for j, s in enumerate(x.get_moves()):
                    if s.check_finished():
                        #print(s)
                        G = s.history_string()
                        # print('-')
                        #print(''.join(G[:-1]), len(G))
                        # print('-')
                        with open(self.filename(), 'w') as file:
                            file.write(G)
                        # input()
                        return ''.join(G)
                    elif hash(s) in self.seenstates:
                        # print('cashed')
                        continue
                    elif State.len_history(hash(s)) > MAX_MOVES:
                        continue
                    else:
                        # print(str(s))
                        # self.deck.append(s)
                        # print(i)
                        # print(s.check_connection())
                        assert not s.check_finished()
                        # print('->',s.check_connection())
                        heapq.heappush(self.q, (s.check_connection(), i + j / 4, s))
        except Exception:
            raise
        finally:
            bar.close()


translate = {
    up: 'U',
    down: 'D',
    left: 'L',
    right: 'R'
}


def solve(state, allowgen):
    s = State(state)
    b = BackTracer(s, 30*60)
    return b.safesearch(allowgen)


if __name__ == '__main__':
    restore = False
    regen = False
    all_ = list(product(range(8), range(5), range(5)))
    shuffle(all_)
    for x, i, j in all_:
        p = Puzzle(x, i, j)
        z = p.get_puzzle()
        if z is None:
            continue
        #print(z)
        s = p.get_solution()
        if (s is None or restore) and regen:
            print(x, i, j)
            try:
                s = solve(z, True)
            except AssertionError:
                continue
            p.set_solution(s)
            print(s)
        if s is not None:
            try:
                s2 = solve(z, False)
            except AssertionError:
                continue
            if s == s2:
                print('mark solution as alternate')
                p.mark_sol_as_alt()
