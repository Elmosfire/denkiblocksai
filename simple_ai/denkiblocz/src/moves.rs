use crate::state::State;
use crate::state::Status;
use std::collections::VecDeque;

#[derive(Default)]
pub struct MovesQueue {
    underlying : VecDeque<u16>,
}

pub struct CompressedMoves {
    size : usize,
    content :Vec<u16>,
}

impl CompressedMoves {
    pub fn from_moves(moves : &Moves) -> Self {
        let mut res = Self {
            size : moves.moves.len(),
            content : Vec::new(),
        };
        for chunk in moves.moves.chunks(8) {
            let sum = chunk.iter().enumerate().map(|(n, &x)| (x as u16) << (2*n)).sum();
            res.content.push(sum)
        }
        res
    }
    pub fn to_moves(self) -> Moves {
        let mut res = Moves::default();
        for mut new in self.content {
            for _ in 0..8 {
                res.moves.push((new & 3) as u8);
                new /= 4;
            }
        }
        res.moves.truncate(self.size);
        res
    }
    pub fn len(&self) -> usize {
        self.size
    }
}

impl MovesQueue {
    pub fn push_c(&mut self, moves: &CompressedMoves) {
        self.underlying.push_back(moves.size as u16);
        self.underlying.extend(&moves.content);
    }
    pub fn pull_c(&mut self) -> Option<CompressedMoves> {
        let size = self.underlying.pop_front()? as usize;
        let len = (size + 7)/8;
        let content = self.underlying.drain(..len).collect();
        Some(CompressedMoves { size, content })
    }
    pub fn push(&mut self, moves : &Moves) {
        self.push_c(&CompressedMoves::from_moves(moves))
    }
    pub fn pull(&mut self) -> Option<Moves> {
        Some(self.pull_c()?.to_moves())
    }
    pub fn len(&self) -> usize {
        self.underlying.len()
    }
}

impl MovesQueue {
    pub fn new() -> Self {
        Self::default()
    }
}
#[derive(Debug, Default, PartialEq, Eq, PartialOrd, Ord, Clone)]
pub struct Moves {
    moves : Vec<u8>,
}

impl Moves {
    pub fn len(&self) -> usize {
        self.moves.len()
    }
    pub fn apply(&self, state: &mut State) -> Status {
        let mut res = Status::default();
        for &k in &self.moves {
            res = state.move_any(k as _);
        }
        res
    }
    pub fn last_move(&self) -> u8 {
        self.moves.last().cloned().unwrap_or(255)
    }
    pub fn to_string(&self) -> String {
        self.moves.iter().map(|&x| b"LRUD"[x as usize] as char).collect()
    }
    pub fn extend<F : FnMut(Moves)>(&self, mut f : F, ret : bool) {
        let prev = self.moves.last().cloned().unwrap_or(0);
        let mut a = self.clone();
        a.moves.push(prev);
        f(a);
        let mut b = self.clone();
        b.moves.push(prev ^ 3);
        f(b);
        let mut c = self.clone();
        c.moves.push(prev ^ 2);
        f(c);
        if ret {
            let mut d = self.clone();
            d.moves.push(prev ^ 1);
            f(d);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let moves = Moves{moves: vec![3,1,3,0,1,3,1,2,0,3]};
        let mut movesqueue = MovesQueue::new();
        movesqueue.push(&moves);
        movesqueue.push(&moves);
        assert_eq!(Some(&moves), movesqueue.pull().as_ref());
        assert_eq!(Some(&moves), movesqueue.pull().as_ref());
        let compressed_moves = CompressedMoves::from_moves(&moves);
        assert_eq!(compressed_moves.to_moves(), moves);
    }
}
