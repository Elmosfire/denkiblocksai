use std::fmt;

const BOARD_SIZE : usize = 16;

#[derive(PartialEq, Eq, Hash, Debug, Clone, PartialOrd, Ord)]
#[repr(C)]
#[repr(align(16))]
pub struct Shape {
    data : [u16; BOARD_SIZE],
    color : usize,
    garbage: bool,
}

type Move = fn (&mut Shape) -> bool;
impl Shape {
    pub fn new(color : usize) -> Self {
        Self {
            color,
            data : [0; BOARD_SIZE],
            garbage: false,
        }
    }
    pub fn collides(&self, other : &Shape) -> bool {
        let mut res = 0;
        for i in 0..16 {
            res |= self.data[i] & other.data[i];
        }
        res > 0
    }

    pub fn passes(&self, other : &Shape) -> bool {
        let mut res = 0;
        let mut k = other.data[0];
        for i in 0..16 {
            let x = other.data[i];
            let x = x | (x<<1) | (x>>1) | k;
            k = other.data[i];
            res |= self.data[i] & x;
        }
        for i in 1..16 {
            res |= self.data[i - 1] & other.data[i];
        }
        res > 0
    }

    pub fn join(&mut self, other : &Shape) {
        for i in 0..16 {
            self.data[i] |= other.data[i];
        }
    }

    pub fn move_down(&mut self) -> bool {
        if self.data[15] > 0 {
            return false;
        }
        for i in 1..16 {
            self.data[16 - i] = self.data[15 - i];
        }
        self.data[0] = 0;
        true
    }
    pub fn move_up(&mut self) -> bool {
        if self.data[0] > 0 {
            return false;
        }
        for i in 0..15 {
            self.data[i] = self.data[i + 1];
        }
        self.data[15] = 0;
        true
    }
    pub fn move_left(&mut self) -> bool {
        for i in 0..16 {
            if self.data[i] & 1 != 0 {
                return false;
            }
        }
        for i in 0..16 {
            self.data[i] >>= 1;
        }
        true
    }
    pub fn move_right(&mut self) -> bool {
        for i in 0..16 {
            if self.data[i] & 0x8000 != 0 {
                return false;
            }
        }
        for i in 0..16 {
            self.data[i] <<= 1;
        }
        true
    }
    pub fn set(&mut self, x : usize, y : usize) {
        self.data[y] |= 1 << x;
    }
    pub fn get(&self, x : usize, y : usize) -> bool {
        self.data[y] & 1 << x > 0
    }
}

#[derive(Default)]
pub struct Status {
    pub revert : bool,
    pub noop : bool,
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct State {
    walls : Shape,
    colors : Vec<Shape>,
}
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct HashState {
    colors : [[u16; 16]; 4],
}

impl State {
    pub fn heuristic(&self) -> usize {
        self.colors.len()
    }
    pub fn to_hashstate(&self) -> HashState {
        let mut res = HashState{colors: [[0;16];4]};
        for c in 0..4 {
            for shape in &self.colors {
                if shape.color == c + 2 {
                    for i in 0..16 {
                        res.colors[c][i] |= shape.data[i];
                    }
                }
            }
        }
        res
    }
}

impl fmt::Display for State {
    fn fmt(&self, f : &mut fmt::Formatter) -> Result<(), fmt::Error> {
        for row in 0..16 {
            for column in 0..16 {
                let mut r = 0;
                self.colors.iter().chain(Some(&self.walls)).for_each(|shape| {
                    if shape.get(column, row) {
                        r = 10 * r + shape.color;
                    }
                });
                let c = b".xrgby"[r] as char;
                fmt::Display::fmt(&c, f)?;
            }
            fmt::Display::fmt(&'\n', f)?;
        }
        Ok(())
    }
}

impl State {
    pub fn from_string(data : &[u8]) -> Self {
        Self::new(data.into_iter().filter_map(|&c| match c {
            b'.' => Some(0),
            b'x' => Some(1),
            b'r' => Some(2),
            b'g' => Some(3),
            b'b' => Some(4),
            b'y' => Some(5),
            _ => None
        }))
    }
    pub fn new(data : impl IntoIterator<Item=u8>) -> Self {
        let mut data = data.into_iter();
        let mut walls = Shape::new(1);
        let mut colors = Vec::new();
        for row in 0..16 {
            for column in 0..16 {
                match data.next() {
                    None => panic!(),
                    Some(0) => {},
                    Some(i) => {
                        let mut shape = Shape::new(i as usize);
                        shape.set(column, row);
                        if i == 1 {
                            walls.join(&shape);
                        } else {
                            colors.push(shape);
                        }
                    }
                }
            }
        }

        let mut res = Self {
            walls, colors
        };
        res.rejoin();
        res
    }

    pub fn solved(&self) -> bool {
        let mut array = &self.colors[..];
        while let Some((a, rest)) = array.split_first() {
            array = rest;
            for b in &array[..] {
                if a.color == b.color {
                    return false;
                }
            }

        }
        true
    }
    pub fn rejoin(&mut self) {
        let mut slice = &mut self.colors[..];
        while let Some((shape_1, remainder)) = slice.split_first_mut() {
            slice = remainder;
            for shape_2 in &mut slice[..] {
                if shape_1.color == shape_2.color && shape_1.passes(&*shape_2) {
                    shape_1.garbage = true;
                    shape_2.join(&*shape_1);
                    break;
                }
            }
        }
        self.colors.retain(|x| !x.garbage);
    }
    pub fn move_any(&mut self, dir : usize) -> Status {
        let (f, undo) : (Move, Move) = match dir {
            0 => (Shape::move_left, Shape::move_right),
            1 => (Shape::move_right, Shape::move_left),
            2 => (Shape::move_up, Shape::move_down),
            3 => (Shape::move_down, Shape::move_up),
            _ => panic!(),
        };
        let mut tumor = self.walls.clone();
        let mut moved_shapes = Vec::with_capacity(self.colors.len());
        let mut remaining_shapes = Vec::with_capacity(self.colors.len());
        for new_shape in &mut self.colors {
            if f(new_shape) {
                moved_shapes.push(new_shape);
            } else {
                tumor.join(&*new_shape);
                remaining_shapes.push(new_shape);
            }
        }
        'l:
        loop {
            for i in 0..moved_shapes.len() {
                if moved_shapes[i].collides(&tumor) {
                    let shape = moved_shapes.remove(i);
                    undo(shape);
                    tumor.join(&*shape);
                    remaining_shapes.push(shape);
                    continue 'l;
                }
            }
            break;
        }
        let status = Status {
            revert : remaining_shapes.is_empty(),
            noop: moved_shapes.is_empty(),
        };
        if !status.noop {
            self.rejoin();
        }
        status
    }
}


#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn it_works() {
        let input = b"................
            ................
            ................
            ................
            ................
            ................
            ......rgy.......
            .xxxxxxxxxxxxxx.
            ......rgy.......
            ................
            ................
            ................
            ................
            ................
            ................
            ................";
        let mut state = State::from_string(input);
        //LLLLLLULULUURDDDDLL
        state.move_any(0);
        state.move_any(0);
        state.move_any(0);
        state.move_any(0);
        state.move_any(0);
        state.move_any(0);
        let x = state.clone();
        state.move_any(0);
        assert_eq!(x, state);
        state.move_any(2);
        state.move_any(0);
        state.move_any(2);
        state.move_any(0);
        state.move_any(2);
        state.move_any(2);
        state.move_any(1);
        state.move_any(3);
        state.move_any(3);
        state.move_any(3);
        state.move_any(3);
        state.move_any(0);
        assert!(!state.solved());
        state.move_any(0);
        assert!(state.solved());
	}
}
