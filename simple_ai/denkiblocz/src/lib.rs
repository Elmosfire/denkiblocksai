use std::collections::VecDeque;
use fnv::FnvHashSet as Set;
use fnv::FnvHasher as UseHasher;
use std::hash::Hasher as HasherTrait;
use std::hash::Hash;
use std::iter;
use std::mem;
use std::sync::Mutex;
use rayon::prelude::*;
pub mod state_naive;
pub use state_naive as state;
pub mod moves;
const RAM : usize = 400000000 * 8;

pub type Error = Box<dyn std::error::Error + 'static + Send + Sync>;

pub fn solve(state: &[u8]) -> Option<(String, String)> {
    let state = state::State::from_string(state);
    let mut passed = Set::default();
    let n = state.heuristic();
    let mut queues : Vec<_> = (0..=n).map(|_| moves::MovesQueue::new()).collect();
    queues[n].push(&moves::Moves::default());
    let now = std::time::Instant::now();
    let mut last_len = 0;
    let t = Mutex::new(None);
    loop {
        if passed.len() * 8 >= RAM {
            println!("Out of memory!");
            return None;
        }
        let index = queues.iter().position(|x| x.len() > 0);
        let values : Vec<_>= {
            queues.iter_mut().filter(|x| x.len() > 0).flat_map(|queue| iter::from_fn(move || queue.pull_c())).take(300000).collect()
        };
        if let Some(m) = &values.get(0) {
            println!("DEBUG: {} {} {}", index.unwrap(), m.len(), passed.len());
        } else {
            return None;
        }
        let res : Vec<_> = values.into_par_iter().map(|m| {
            let m = m.to_moves();
            let mut queue_entries = Vec::new();
            let mut state = state.clone();
            let ret = m.apply(&mut state);
            if state.solved() {
                println!("{}", passed.len() * 8);
                println!("{:?}", std::time::Instant::now() - now);
                *t.lock().unwrap() = Some((m.to_string(), state.to_string()));
            }
            m.extend(|x| {
                let mut state = state.clone();
                state.move_any(x.last_move() as usize);
                let mut hasher = UseHasher::with_key(0xDEADBEEF);
                state.to_hashstate().hash(&mut hasher);
                let h = hasher.finish();
                if !passed.contains(&h) {
                    queue_entries.push((h, state.heuristic(), moves::CompressedMoves::from_moves(&x)));
                }
            }, true);
            queue_entries
        }).collect();
        if let Some(x) = mem::replace(&mut *t.lock().unwrap(), None) {
            return Some(x);
        }
        for el in res {
            for (hash, heur, entry) in el {
                if passed.insert(hash) {
                    queues[heur].push_c(&entry);
                }
            }
        }
    }
}

