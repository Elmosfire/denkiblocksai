use denkiblocz::solve;
use redis::Client;
use redis::Commands;
type Error = Box<dyn std::error::Error + 'static>;

fn print_puzzle(puzzle : &str) {
    let mut res = String::new();
    for i in puzzle.chars() {
        match i {
            '.' => res.push_str("\u{001b}[40;1m\u{001b}[30m#"),
            'x' => res.push_str("\u{001b}[47;1m\u{001b}[37m#"),
            'r' => res.push_str("\u{001b}[41;1m\u{001b}[31m#"),
            'g' => res.push_str("\u{001b}[42;1m\u{001b}[32m#"),
            'y' => res.push_str("\u{001b}[43;1m\u{001b}[33m#"),
            'b' => res.push_str("\u{001b}[44;1m\u{001b}[34m#"),
              _ => res.push_str("\u{001b}[0m"),
        };
        res.push(i);
    }
    println!("{}", res);
    println!("\u{001b}[0m");
}

fn main() -> Result<(), Error> {
    let url = std::env::var("DENKIURL")?;
    let pass = std::env::var("DENKIPASS")?;
    let client = Client::open(&url)?;
    let mut con = client.get_connection()?;
    redis::cmd("AUTH")
        .arg(&pass)
        .query(&mut con)?;

    for x in 7..8 {
        for y in 0..5 {
            for z in 0..5 {
                let previous_solution: String = con.get(&format!("puzzle:{}:{}:{}:solution", x, y, z)).unwrap_or(String::new());
                if previous_solution.len() > 0 {
                    println!("Puzzle {}:{}:{} has solution {}", x, y, z, previous_solution);
                    continue;
                }
                println!("puzzle:{}:{}:{}:puzzle", x, y, z);
                let puzzle: Result<String, _> = con.get(&format!("puzzle:{}:{}:{}:puzzle", x, y, z));
                if let Ok(puzzle) = puzzle {
                    print_puzzle(&puzzle);
                    if let Some((res, k)) = solve(&puzzle.as_bytes()) {
                        println!("{}", res);
                        print_puzzle(&k);
                        if previous_solution.len() > 0 {
                            continue;
                        }
                        con.set(&format!("puzzle:{}:{}:{}:solution", x, y, z), res)?;
                    }
                }
            }
        }
    }
    println!("Tried to solve them all!");
    Ok(())
}
